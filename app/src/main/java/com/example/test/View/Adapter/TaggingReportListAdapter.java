package com.example.test.View.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.Tagging;
import com.example.test.Presenter.TaggingMasterPresenter;
import com.example.test.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TaggingReportListAdapter extends ArrayAdapter<String> {
    Context context;
    List<Tagging> values;
    TextView serialNum;
    TextView desc;
    ImageView imageView;

    public TaggingReportListAdapter(@NonNull Context context, @NonNull List objects) {
        super(context, R.layout.row_layout_tagging_report, objects);
        this.context = context;
        this.values = objects;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_layout_tagging_report, parent, false);
        imageView = view.findViewById(R.id.imageListTaggingReport);
        serialNum = view.findViewById(R.id.serialnumberListTaggingReport);
        desc = view.findViewById(R.id.textListTaggingReport);
        if (values.get(position).getUrlPhoto() != null) {
            String imgUrl = values.get(position).getUrlPhoto();
            Picasso.get().load(imgUrl.replace("localhost", APIUtils.URL)).into(imageView);
        }
        serialNum.setText(values.get(position).getSerialNumber());
        Date date = values.get(position).getTaggingDate();
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        desc.setText(simpleDateFormat.format(date));

        return view;
    }

}

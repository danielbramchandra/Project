package com.example.test.View.interfaces;

import com.example.test.Model.SurveyAnswer;

import java.util.List;

public interface ViewObject {
    void updateObjectList(List<Object> object);
}

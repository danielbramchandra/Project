package com.example.test.View.BaseHomeNavBarFragments.CrudMasterFragments;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.test.View.Adapter.CustomerAdapter;
import com.example.test.Model.Customer;
import com.example.test.Model.User;
import com.example.test.Presenter.CustomerMasterPresenter;
import com.example.test.Presenter.UserMasterPresenter;
import com.example.test.View.interfaces.ViewCustomer;
import com.example.test.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.LinkedList;
import java.util.List;


public class CustomerMasterFragment extends Fragment implements ViewCustomer {
    FloatingActionButton floatingActionButton;
    CustomerMasterPresenter presenterCustomer;
    List<Customer> dataCustomer;
    List<User> dataUser;
    ListView listViewCustomer;
    CustomerAdapter customAdapter;

    public CustomerMasterFragment() {
        presenterCustomer = new CustomerMasterPresenter(this);
        dataUser = new LinkedList();
        dataCustomer = new LinkedList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_customer_master, container, false);
        floatingActionButton = view.findViewById(R.id.floatingButtonCustomerMaster);
        presenterCustomer.getCustomerData();
        listViewCustomer = view.findViewById(R.id.listCustomerMaster);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogForm();
            }
        });
        return view;
    }

    private void DialogForm() {
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.form_data_customer, null);
        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setView(dialogView)
                .setTitle("Add Customer")
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Save", null)
                .show();
        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        Button negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText name = dialogView.findViewById(R.id.customerMasterNameField);
                EditText address = dialogView.findViewById(R.id.customerMasterAddressField);
                EditText email = dialogView.findViewById(R.id.customerMasterEmailField);
                EditText phoneNumber = dialogView.findViewById(R.id.customerMasterPhoneNumberField);
                if (name.getText().toString().length() == 0 && address.getText().toString().length() == 0 && email.getText().toString().length() == 0 && phoneNumber.getText().toString().length() == 0) {
                    Toast toast = Toast.makeText(getContext(), "Complete The Blank Form", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    if (!email.getText().toString().matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$")) {
                        email.requestFocus();
                        email.setError("Please enter the correct format of email");
                    } else if (!phoneNumber.getText().toString().matches("^[0-9]{0,14}$")) {
                        phoneNumber.requestFocus();
                        phoneNumber.setError("Insert max 14 number");
                    } else {
                        Customer customer = new Customer();
                        customer.setName(name.getText().toString());
                        customer.setAddress(address.getText().toString());
                        customer.setEmail(email.getText().toString());
                        customer.setPhoneNumber(phoneNumber.getText().toString());
                        presenterCustomer.saveCustomerData(customer);
                        dialog.dismiss();
                    }
                }
            }
        });
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void updateCustomerInfoListView(List customer) {
        customAdapter = new CustomerAdapter(getContext(), customer, presenterCustomer);
        listViewCustomer.setAdapter(customAdapter);
        dataCustomer = customer;
        System.out.println(customer);
        customAdapter.notifyDataSetChanged();
    }

}
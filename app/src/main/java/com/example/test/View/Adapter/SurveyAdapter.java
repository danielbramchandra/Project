package com.example.test.View.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.example.test.Model.Customer;
import com.example.test.Model.Survey;
import com.example.test.Presenter.SurveyMasterPresenter;
import com.example.test.R;

import java.util.ArrayList;
import java.util.List;

public class SurveyAdapter extends ArrayAdapter<String> {
    private Context context;
    private List<Survey> values;
    SurveyMasterPresenter surveyMasterPresenter;
    TextView textView;
    AlertDialog.Builder dialog;
    ImageButton deleteButton;
    View dialogView;
    EditText question;

    ImageButton editButton;
    Spinner type;
    List listType;

    public SurveyAdapter(@NonNull Context context, @NonNull List objects, SurveyMasterPresenter surveyMasterPresenter) {
        super(context, R.layout.row_layout_crud, objects);
        this.context = context;
        this.values = objects;
        this.surveyMasterPresenter = surveyMasterPresenter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_layout_crud, parent, false);
        textView = view.findViewById(R.id.textRow);
        textView.setTextSize(15);
        textView.setTextColor(ContextCompat.getColor(context, R.color.black));
        deleteButton = view.findViewById(R.id.deleteRow);
        editButton = view.findViewById(R.id.editRow);
        displayText(values.get(position), inflater);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editData(values.get(position), inflater);
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                dialog.setMessage("Delete Data?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteData(values.get(position));
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        return view;

    }

    public void displayText(Survey value, LayoutInflater inflater) {
        textView.setText(value.getSurveyQuestion());
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog = new AlertDialog.Builder(context);
                dialogView = inflater.inflate(R.layout.form_add_survey, null);
                LinearLayout rootLayout = dialogView.findViewById(R.id.rootView);
                listType = new ArrayList();
                listType.add("FreeText");
                listType.add("RadioButton");
                listType.add("CheckBox");
                question = dialogView.findViewById(R.id.addQuestion);
                type = dialogView.findViewById(R.id.typeOption);
                question.setText(value.getSurveyQuestion());
                type.setAdapter(new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, listType));
                type.setSelection(getIndex(type, value.getType()));
                String option = value.getSurveyOption();
                question.setBackground(null);
                question.setFocusable(false);
                type.setBackground(null);
                type.setFocusable(false);
                if (!value.getType().equals("FreeText")) {
                    TextView textView = new TextView(getContext());
                    textView.setText("Option");
                    rootLayout.addView(textView);
                    String[] optionArr = option.split(";");
                    for (int i = 0; i < optionArr.length; i++) {
                        EditText editText = new EditText(getContext());
                        editText.setText(optionArr[i]);
                        rootLayout.addView(editText);
                    }
                }

                dialog.setView(dialogView);
                dialog.setTitle("Details Customer");
                dialog.show();
            }
        });
    }

    public void deleteData(Survey value) {
        surveyMasterPresenter.deleteSurveyData(value);
    }

    public void editData(Survey value, LayoutInflater inflater) {
        dialog = new AlertDialog.Builder(context);
        dialogView = inflater.inflate(R.layout.form_add_survey, null);
        LinearLayout rootLayout = dialogView.findViewById(R.id.rootView);
        listType = new ArrayList();
        listType.clear();
        listType.add("FreeText");
        listType.add("RadioButton");
        listType.add("CheckBox");
        question = dialogView.findViewById(R.id.addQuestion);
        type = dialogView.findViewById(R.id.typeOption);
        type.setAdapter(new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, listType));
        type.setSelection(getIndex(type, value.getType()));
        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!listType.get(position).toString().equalsIgnoreCase("FreeText")) {
                    rootLayout.removeAllViews();
                    Button add = new Button(getContext());
                    add.setText("Add");
                    TextView textView = new TextView(getContext());
                    textView.setText("Option");
                    EditText editText = new EditText(getContext());
                    rootLayout.addView(add);
                    rootLayout.addView(textView);
                    rootLayout.addView(editText);
                    add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (rootLayout.getChildCount() < 6) {
                                EditText editText = new EditText(getContext());
                                rootLayout.addView(editText);
                            }
                        }
                    });
                    if (listType.get(position).toString().matches(value.getType())) {
                        rootLayout.removeAllViews();
                        rootLayout.addView(add);
                        rootLayout.addView(textView);
                        String option = value.getSurveyOption();
                        if (option != null) {
                            String[] optionArr = option.split(";");
                            for (int i = 0; i < optionArr.length; i++) {
                                editText = new EditText(getContext());
                                editText.setText(optionArr[i]);
                                rootLayout.addView(editText);
                            }
                        }
                    }
                } else {
                    rootLayout.removeAllViews();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        question.setText(value.getSurveyQuestion());
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ViewGroup viewGroup = (ViewGroup) rootLayout;
                String str = "";
                for (int i = 0; i < viewGroup.getChildCount(); i++) {
                    View viewChild = viewGroup.getChildAt(i);
                    if (viewChild instanceof EditText) {
                        str += ((EditText) viewChild).getText().toString();
                        if (viewGroup.getChildCount() - i != 1) {
                            str += ";";
                        }
                    }

                }
                value.setSurveyQuestion(question.getText().toString());
                value.setType(type.getSelectedItem().toString());
                value.setSurveyOption(str);
                surveyMasterPresenter.saveSurveyData(value);
            }
        });
        dialog.setView(dialogView);
        dialog.setTitle("Edit Survey");
        dialog.show();
    }

    public int getIndex(Spinner spinner, String s) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(s)) {
                return i;
            }
        }
        return 0;
    }
}

package com.example.test.View.BaseHomeNavBarFragments.ReportFragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.test.View.Adapter.TaggingReportListAdapter;
import com.example.test.Model.Tagging;
import com.example.test.Model.User;
import com.example.test.Presenter.TaggingMasterPresenter;
import com.example.test.Presenter.UserMasterPresenter;
import com.example.test.View.interfaces.ViewTagging;
import com.example.test.View.interfaces.ViewUser;
import com.example.test.R;

import java.util.ArrayList;
import java.util.List;


public class TaggingReportList extends Fragment implements ViewTagging, ViewUser {
    ListView listView;
    TaggingMasterPresenter taggingMasterPresenter;
    UserMasterPresenter userMasterPresenter;
    TaggingReportListAdapter taggingReportListAdapter;
    String uname;
    List<Tagging> taggings;
    public TaggingReportList(){
        taggingMasterPresenter = new TaggingMasterPresenter(this);
        userMasterPresenter = new UserMasterPresenter(this);
        taggings = new ArrayList<>();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tagging_report_list, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            uname = bundle.getString("usernameSales");
            userMasterPresenter.findByUsername(uname);
        }
        listView = view.findViewById(R.id.listUserTaggingReport);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity(), TaggingReportDetailsActivity.class);
                i.putExtra("Tagging", String.valueOf(taggings.get(position).getId()));
                startActivity(i);
            }
        });
        return view;
    }

    @Override
    public void updateTaggingList(List<Tagging> tagging) {
        taggingReportListAdapter = new TaggingReportListAdapter(getContext(),tagging);
        listView.setAdapter(taggingReportListAdapter);
        taggings = tagging;
    }

    @Override
    public void updateUserList(List<User> user) {
        taggingMasterPresenter.getTaggingDataByUser(user.get(0));
    }
}
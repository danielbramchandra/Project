package com.example.test.View.interfaces;

import com.example.test.Model.Survey;

import java.util.List;

public interface ViewSurvey {
    void updateSurveyList(List<Survey> survey);
}

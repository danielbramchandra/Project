package com.example.test.View.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.User;
import com.example.test.Presenter.UserMasterPresenter;
import com.example.test.R;
import com.example.test.View.BaseHomeNavBarFragments.CrudMasterFragments.UserMasterFragment;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UserAdapter extends ArrayAdapter<String> {
    private Context context;
    private List<User> values;
    TextView textView;
    UserMasterPresenter userMasterPresenter;
    UserMasterFragment userMasterFragment;
    AlertDialog.Builder dialog;
    ImageButton deleteButton;
    ImageButton editButton;
    Activity activity ;
    Uri image;
    public UserAdapter(@NonNull Context context, @NonNull List objects, UserMasterPresenter userMasterPresenter, UserMasterFragment fragment) {
        super(context, R.layout.row_layout_crud, objects);
        this.context = context;
        this.values = objects;
        this.userMasterPresenter = userMasterPresenter;
        activity = (Activity) context;
        this.userMasterFragment = fragment;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_layout_crud,parent,false);
        textView = view.findViewById(R.id.textRow);
        textView.setTextSize(15);
        textView.setTextColor(ContextCompat.getColor(context, R.color.black));
        deleteButton = view.findViewById(R.id.deleteRow);
        editButton = view.findViewById(R.id.editRow);
        displayText(values.get(position),inflater);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editData(values.get(position),inflater);
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                dialog.setMessage("Delete Data?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteData(values.get(position));
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        return view;

    }
    public  void displayText(User value,LayoutInflater inflater){
        textView.setText(value.getName());
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogUser dialogUser = new DialogUser(context,inflater);
                dialogUser.setData(value);
                dialogUser.setNotClickable();
                if(value.getUrlPhoto()!=null){
                    Picasso.get().load(value.getUrlPhoto().replace("localhost", APIUtils.URL)).into(dialogUser.getPicture());
                }
                dialogUser.getProfilePic().setVisibility(View.GONE);
                dialogUser.getBrowseProfilePic().setVisibility(View.GONE);
                dialogUser.show();
            }
        });
    }
    public  void  deleteData(User value){
        userMasterPresenter.deleteUser(value.getId());
    }
    public void editData(User value,LayoutInflater inflater){
        userMasterFragment.DialogForm("update");
        userMasterFragment.setData(value);
    }

}

package com.example.test.View.BaseHomeNavBarFragments.CrudMasterFragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.test.Model.User;
import com.example.test.Presenter.UserMasterPresenter;
import com.example.test.View.interfaces.ViewUser;
import com.example.test.R;
import com.example.test.View.BaseHomeNavBarFragments.CrudMasterFragments.Survey.AddSurvey;

import java.util.ArrayList;
import java.util.List;


public class SurveyMasterFragment extends Fragment implements ViewUser {
    ListView listView;
    ArrayAdapter adapter;
    List<User> listUser;
    UserMasterPresenter presenterSales;
    public SurveyMasterFragment(){
        presenterSales = new UserMasterPresenter(this);
        listUser = new ArrayList<>();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_survey_master, container, false);
        listView = view.findViewById(R.id.listUserSurveyMaster);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AddSurvey nextFrag = new AddSurvey();
                Bundle bundle = new Bundle();
                bundle.putInt("User",listUser.get(position).getId());
                nextFrag.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_container, nextFrag)
                        .addToBackStack(null)
                        .commit();
            }
        });
        presenterSales.getUserHaveRoleSurvey();
        return view;
    }
    public List getUserName(List<User> data){
        List temp = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            temp.add(data.get(i).getName());
        }
        return temp;
    }
    @Override
    public void updateUserList(List<User> user) {
        adapter = new ArrayAdapter(getContext(),R.layout.support_simple_spinner_dropdown_item,getUserName(user));
        listView.setAdapter(adapter);
        listUser = user;
        adapter.notifyDataSetChanged();
    }
}
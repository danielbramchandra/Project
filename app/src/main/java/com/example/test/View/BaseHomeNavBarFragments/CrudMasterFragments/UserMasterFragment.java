package com.example.test.View.BaseHomeNavBarFragments.CrudMasterFragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.test.View.Adapter.UserAdapter;
import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.User;
import com.example.test.Presenter.UserMasterPresenter;
import com.example.test.View.interfaces.ViewUser;
import com.example.test.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static android.app.Activity.RESULT_OK;


public class UserMasterFragment extends Fragment implements ViewUser {
    List<User> data;
    String[] genderArray = {"Male","Female"};
    String[] roleArray = {"Survey","Tagging"};
    ListView listView;
    EditText name;
    EditText username;
    EditText password;
    EditText phoneNumber;
    EditText email;
    UserAdapter customAdapter;
    FloatingActionButton button;
    UserMasterPresenter presenter;
    ImageView imageView;
    Spinner gender;
    Spinner role;
    int id;
    View dialogView;
    public Uri imageUri;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private static final int PICK_IMAGE = 1;
    public UserMasterFragment() {
        presenter = new UserMasterPresenter(this);
        data = new LinkedList();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_user_master, container, false);
        button = view.findViewById(R.id.userMasterfloatingActionButton);
        presenter.getUserData();
        listView = view.findViewById(R.id.listUserMaster);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogForm("save");
            }
        });

        return view;
    }
    public List getUser(List<User> data){
        List temp = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
                temp.add(data.get(i).getName());
        }
        return temp;
    }

    @Override
    public void updateUserList(List<User> user) {
        customAdapter = new UserAdapter(getContext(),user,presenter, this);
//        adapter = new ArrayAdapter(getActivity(),R.layout.support_simple_spinner_dropdown_item,getUser(user));
        listView.setAdapter(customAdapter);
        data = user;
        customAdapter.notifyDataSetChanged();
    }
    public void DialogForm(String type){
        LayoutInflater inflater = getLayoutInflater();
         dialogView = inflater.inflate(R.layout.form_data_user, null);
        AlertDialog dialog = new AlertDialog.Builder(getContext())
                .setTitle("Add Sales")
                .setPositiveButton("Save",null)
                .setNegativeButton("Cancel",null)
                .setView(dialogView)
                .show();
        name = dialogView.findViewById(R.id.userMasterNameField);
        username = dialogView.findViewById(R.id.userMasterUsernameField);
        password = dialogView.findViewById(R.id.userMasterPasswordField);
        phoneNumber = dialogView.findViewById(R.id.userMasterPhoneNumberField);
        email = dialogView.findViewById(R.id.userMasterEmailField);
        imageView = dialogView.findViewById(R.id.profilePicture);
        gender = dialogView.findViewById(R.id.userMasterGenderSpinner);
        role = dialogView.findViewById(R.id.userMasterRoleSpinner);
        gender.setAdapter(new ArrayAdapter<>(getContext(),R.layout.support_simple_spinner_dropdown_item,genderArray));
        role.setAdapter(new ArrayAdapter<>(getContext(),R.layout.support_simple_spinner_dropdown_item,roleArray));
        Button button = dialogView.findViewById(R.id.browseImageButton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGallery();
            }
        });

        Button positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE);
        Button negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE);
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (name.getText().toString().length() == 0 &&
                        username.getText().toString().length() == 0 &&
                        password.getText().toString().length() == 0 &&
                        email.getText().toString().length() == 0 &&
                        phoneNumber.getText().toString().length() == 0) {
                    Toast toast = Toast.makeText(getContext(),"Complete The Blank Form",Toast.LENGTH_SHORT);
                    toast.show();
                }else {
                    if(!email.getText().toString().matches("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+$")){
                        email.requestFocus();
                        email.setError("Please enter the correct format of email");
                    }else if (!phoneNumber.getText().toString().matches("^[0-9]{0,14}$")){
                        phoneNumber.requestFocus();
                        phoneNumber.setError("Insert max 14 number");
                    }else {
                        if(type.equals("save")){
                            saveNewUser();
                        }else{
                            updateUser();
                        }
                       dialog.dismiss();
                    }
                }

            }
        });
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
    }

    public void openGallery() {
        verifyStoragePermissions(getActivity());
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);
    }
    public int getIndex(Spinner spinner, String s){
        for (int i = 0; i < spinner.getCount(); i++) {
            if(spinner.getItemAtPosition(i).toString().equalsIgnoreCase(s)){
                return i;
            }
        }
        return 0;
    }
    public void saveNewUser(){
        User user = new User();
        user.setName(name.getText().toString());
        user.setUsername(username.getText().toString());
        user.setPassword(password.getText().toString());
        user.setEmail(email.getText().toString());
        user.setPhoneNumber(phoneNumber.getText().toString());
        user.setGender(gender.getSelectedItem().toString());
        user.setRole(role.getSelectedItem().toString());
        if(imageUri==null){
            presenter.saveUserData(user,getContext());
        }else{
            user.setUrlPhoto(imagePath(imageUri));
            presenter.saveUserDataWithPicture(user,imagePath(imageUri));
        }
    }
    public void updateUser(){
        User user = new User();
        user.setId(id);
        user.setName(name.getText().toString());
        user.setUsername(username.getText().toString());
        user.setPassword(password.getText().toString());
        user.setEmail(email.getText().toString());
        user.setPhoneNumber(phoneNumber.getText().toString());
        user.setGender(gender.getSelectedItem().toString());
        user.setRole(role.getSelectedItem().toString());
        if(imageUri==null){
            presenter.saveUserData(user,getContext());
        }else{
            presenter.saveUserDataWithPicture(user,imagePath(imageUri));
        }
    }
    public void setData(User user){
        id = user.getId();
        gender.setSelection(getIndex(gender,user.getGender()));
        role.setSelection(getIndex(role,user.getRole()));
        name.setText(user.getName());
        username.setText(user.getUsername());
        password.setText(user.getPassword());
        if(user.getUrlPhoto()!=null){
            Picasso.get().load(user.getUrlPhoto().replace("localhost", APIUtils.URL)).into(imageView);
        }
        phoneNumber.setText(user.getPhoneNumber());
        email.setText(user.getEmail());
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            imageUri = data.getData();
            imageView.setImageURI(imageUri);
        }

    }
    public String imagePath(Uri uri){
        String[] imageProjection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(uri, imageProjection, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            int indexImage = cursor.getColumnIndex(imageProjection[0]);
            return cursor.getString(indexImage);
        }
        return "";
    }
    public static void verifyStoragePermissions(Activity activity) {
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }
    }
}
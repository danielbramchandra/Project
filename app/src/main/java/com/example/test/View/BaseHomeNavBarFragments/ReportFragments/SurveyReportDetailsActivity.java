package com.example.test.View.BaseHomeNavBarFragments.ReportFragments;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.example.test.View.Adapter.SurveyReportListAdapter;
import com.example.test.Model.SurveyAnswer;
import com.example.test.Presenter.SurveyAnswerPresenter;
import com.example.test.View.interfaces.ViewSurveyAnswer;
import com.example.test.R;

import java.util.ArrayList;
import java.util.List;

public class SurveyReportDetailsActivity extends AppCompatActivity implements ViewSurveyAnswer {
    ListView listView;
    TextView userData;
    TextView customerData;
    String user;
    String customer;
    String date;
    SurveyAnswerPresenter surveyAnswerPresenter;
    List<SurveyAnswer> answerList;
    SurveyReportListAdapter surveyReportListAdapter;

    public SurveyReportDetailsActivity() {
        surveyAnswerPresenter = new SurveyAnswerPresenter(this);
        answerList = new ArrayList<>();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_report_details);
        Intent intent = getIntent();
        user = intent.getStringExtra("usernameSales");
        customer = intent.getStringExtra("customer");
        date = intent.getStringExtra("date");
        userData = findViewById(R.id.userText);
        customerData = findViewById(R.id.customerText);
        listView = findViewById(R.id.listSurveyDataReport);

        surveyAnswerPresenter.getAnswerByCustomerAndUser(user, customer, date);

    }

    @Override
    public void updateSurveyAnswerList(List<SurveyAnswer> survey) {
        surveyReportListAdapter = new SurveyReportListAdapter(getApplicationContext(), survey);
        listView.setAdapter(surveyReportListAdapter);
        userData.setText("Sales : " + survey.get(0).getSurvey().getUser().getName());
        userData.setTextSize(15);
        customerData.setText("Customer : " + survey.get(0).getCustomer().getName());
        customerData.setTextSize(15);
    }
}
package com.example.test.View.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.example.test.Model.Customer;
import com.example.test.Presenter.CustomerMasterPresenter;
import com.example.test.R;

import java.util.ArrayList;
import java.util.List;

public class CustomerAdapter extends ArrayAdapter<String>{
    private Context context;
    private List<Customer> values;
    TextView textView;
    CustomerMasterPresenter customerMasterPresenter;
    AlertDialog.Builder dialog;
    ImageButton deleteButton;
    ImageButton editButton;
    public CustomerAdapter(@NonNull Context context, @NonNull List objects, CustomerMasterPresenter customerMasterPresenter) {
        super(context, R.layout.row_layout_crud, objects);
        this.context = context;
        this.values = objects;
        this.customerMasterPresenter = customerMasterPresenter;
    }
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater =(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_layout_crud,parent,false);
        textView = view.findViewById(R.id.textRow);
        textView.setTextSize(15);
        textView.setTextColor(ContextCompat.getColor(context, R.color.black));
        deleteButton = view.findViewById(R.id.deleteRow);
        editButton = view.findViewById(R.id.editRow);
        displayText(values.get(position),inflater);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editData(values.get(position),inflater);
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                dialog.setMessage("Delete Data?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteData(values.get(position));
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        return view;

    }
    public  void displayText(Customer value,LayoutInflater inflater){

        textView.setText(value.getName());
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogCustomer dialogCustomer = new DialogCustomer(context,inflater);
                dialogCustomer.setData(value);
                dialogCustomer.setNotClickable();
                dialogCustomer.show();
            }
        });
    }
    public  void  deleteData(Customer value){

        customerMasterPresenter.deleteCustomer(value.getId());
    }
    public void editData(Customer value,LayoutInflater inflater){
        Customer temp = (Customer) value;
        DialogCustomer dialogCustomer = new DialogCustomer(context,inflater);
        dialogCustomer.setData(temp);
        dialogCustomer.getDialog().setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                temp.setName(dialogCustomer.getEditTextName().getText().toString());
                temp.setAddress(dialogCustomer.getEditTextAddress().getText().toString());
                temp.setEmail(dialogCustomer.getEditTextEmail().getText().toString());
                temp.setPhoneNumber(dialogCustomer.getEditTextPhoneNumber().getText().toString());
                customerMasterPresenter.saveCustomerData(temp);
            }
        });
        dialogCustomer.show();
    }
    public List<Customer> getUsersName(List<Customer> data){
        List temp = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            temp.add(data.get(i).getName());
        }
        return temp;
    }

}

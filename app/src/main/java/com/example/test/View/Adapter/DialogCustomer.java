package com.example.test.View.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.example.test.Model.Customer;
import com.example.test.R;

public class DialogCustomer {
    AlertDialog.Builder dialog;
    View dialogView;
    EditText name;
    EditText address;
    EditText email;
    EditText phoneNumber;
    DialogCustomer(Context context, LayoutInflater inflater){
        dialog = new AlertDialog.Builder(context);
        dialogView = inflater.inflate(R.layout.form_data_customer, null);
         name = dialogView.findViewById(R.id.customerMasterNameField);
         address = dialogView.findViewById(R.id.customerMasterAddressField);
         email = dialogView.findViewById(R.id.customerMasterEmailField);
         phoneNumber = dialogView.findViewById(R.id.customerMasterPhoneNumberField);
         dialog.setView(dialogView);
         dialog.setTitle("Details Customer");
    }
    public  void setData(Customer customer){
        name.setText(customer.getName());
        address.setText(customer.getAddress());
        email.setText(customer.getEmail());
        phoneNumber.setText(customer.getPhoneNumber());
    }
    public void setNotClickable(){
        name.setBackground(null);
        address.setBackground(null);
        email.setBackground(null);
        phoneNumber.setBackground(null);
        name.setFocusable(false);
        address.setFocusable(false);
        email.setFocusable(false);
        phoneNumber.setFocusable(false);

    }

    public EditText getEditTextName() {
        return name;
    }

    public EditText getEditTextAddress() {
        return address;
    }

    public EditText getEditTextEmail() {
        return email;
    }

    public EditText getEditTextPhoneNumber() {
        return phoneNumber;
    }

    public AlertDialog.Builder getDialog() {
        return dialog;
    }

    public View getDialogView() {
        return dialogView;
    }
    public  void show(){
        dialog.show();
    }
}

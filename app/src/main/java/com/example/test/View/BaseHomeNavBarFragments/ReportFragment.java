package com.example.test.View.BaseHomeNavBarFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.test.R;
import com.example.test.View.BaseHomeNavBarFragments.ReportFragments.SurveyReportFragment;
import com.example.test.View.BaseHomeNavBarFragments.ReportFragments.TaggingReportFragment;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class ReportFragment extends Fragment {
    CardView survey;
    CardView tagging;

    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_report, container, false);
        survey = view.findViewById(R.id.surveyReportCard);
        tagging = view.findViewById(R.id.taggingReportCard);
        survey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SurveyReportFragment nextFrag = new SurveyReportFragment();
                loadFragment(nextFrag);
            }
        });
        tagging.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TaggingReportFragment nextFrag = new TaggingReportFragment();
                loadFragment(nextFrag);
            }
        });
        return view;
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container, fragment)
                    .addToBackStack(null)
                    .commit();
            return true;
        }
        return false;
    }
}

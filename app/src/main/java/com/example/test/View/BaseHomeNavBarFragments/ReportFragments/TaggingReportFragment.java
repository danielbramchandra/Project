package com.example.test.View.BaseHomeNavBarFragments.ReportFragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.test.Model.User;
import com.example.test.Presenter.UserMasterPresenter;
import com.example.test.View.interfaces.ViewUser;
import com.example.test.R;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class TaggingReportFragment extends Fragment implements ViewUser {
    ListView listView;
    List<User> userData;
    UserMasterPresenter presenter;
    public TaggingReportFragment() {
        userData = new LinkedList<>();
        presenter = new UserMasterPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tagging_report, container, false);
        listView = view.findViewById(R.id.listTaggingReport);
        presenter.getUserHaveRoleTagging();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TaggingReportList nextFrag = new TaggingReportList();
                Bundle bundle = new Bundle();
                bundle.putString("usernameSales",userData.get(position).getUsername());
                nextFrag.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_container, nextFrag)
                        .addToBackStack(null)
                        .commit();
            }
        });
        return view;
    }
    public List getName(List<User> data){
        List temp = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            temp.add(data.get(i).getName());
        }
        return temp;
    }


    @Override
    public void updateUserList(List<User> user) {
        listView.setAdapter(new ArrayAdapter<>(getContext(),R.layout.support_simple_spinner_dropdown_item,getName(user)));
        userData = user;
    }
}
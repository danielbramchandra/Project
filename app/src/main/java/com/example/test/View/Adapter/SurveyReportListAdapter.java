package com.example.test.View.Adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.test.Model.SurveyAnswer;
import com.example.test.R;

import java.util.Arrays;
import java.util.List;

public class SurveyReportListAdapter extends ArrayAdapter<String> {
    Context context;
    List<SurveyAnswer> values;
    TextView question;

    public SurveyReportListAdapter(@NonNull Context context, @NonNull List objects) {
        super(context, R.layout.row_layout_tagging_report, objects);
        this.context = context;
        this.values = objects;

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_layout_survey_report, parent, false);
        question = view.findViewById(R.id.question);
        question.setTypeface(null, Typeface.BOLD);
        question.setText(values.get(position).getSurvey().getSurveyQuestion());
        LinearLayout rootLayout = view.findViewById(R.id.answer);
        if (values.get(position).getSurvey().getType().equals("FreeText")) {
            TextView textView = new TextView(getContext());
            textView.setText(values.get(position).getAnswer());
            textView.setClickable(false);
            rootLayout.addView(textView);
        } else if (values.get(position).getSurvey().getType().equals("RadioButton")) {
            String strAnswer = values.get(position).getAnswer();
            String[] strOption = values.get(position).getSurvey().getSurveyOption().split(";");
            RadioGroup radioGroup = new RadioGroup(getContext());
            for (int i = 0; i < strOption.length; i++) {
                RadioButton radioButton = new RadioButton(getContext());
                radioButton.setText(strOption[i]);
                if (strOption[i].equals(strAnswer)) {
                    radioButton.setChecked(true);
                }
                radioButton.setClickable(false);
                radioGroup.addView(radioButton);
            }
            rootLayout.addView(radioGroup);
        } else if (values.get(position).getSurvey().getType().equals("CheckBox")) {
            String[] strAnswer = values.get(position).getAnswer().split(";");
            String[] strOption = values.get(position).getSurvey().getSurveyOption().split(";");

            for (int i = 0; i < strOption.length; i++) {
                CheckBox checkBox = new CheckBox(getContext());
                checkBox.setText(strOption[i]);
                if (Arrays.asList(strAnswer).contains(strOption[i])) {
                    checkBox.setChecked(true);
                }
                checkBox.setClickable(false);
                rootLayout.addView(checkBox);
            }
        }
        return view;
    }
}

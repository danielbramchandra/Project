package com.example.test.View.BaseHomeNavBarFragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.test.R;
import com.example.test.View.BaseHomeNavBarFragments.HomeFragments.UbahPasswordFragment;
import com.example.test.View.MainActivity;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class HomeFragment extends Fragment {
    Button ubahPassword;
    Button logout;
    TextView name;
    TextView dateNow;
    @Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable  Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home,container,false);
        SharedPreferences sharedPreferences = this.getContext().getSharedPreferences("UserLogin", Context.MODE_PRIVATE);
        String credential =  sharedPreferences.getString("username", "");
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMMM dd yyyy");
        dateNow = view.findViewById(R.id.dateNow);
        name = view.findViewById(R.id.welcomeName);
        dateNow.setText(simpleDateFormat.format(date));
        name.setText(credential);
        ubahPassword = view.findViewById(R.id.ubahPasswordButton);
        ubahPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UbahPasswordFragment nextFrag= new UbahPasswordFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fl_container, nextFrag)
                        .addToBackStack(null)
                        .commit();
            }
        });
        logout = view.findViewById(R.id.logoutButton);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences.edit().clear().commit();
                Intent i = new Intent(getActivity(), MainActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i);
            }
        });
        return view;

    }

}

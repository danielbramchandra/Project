package com.example.test.View.BaseHomeNavBarFragments.CrudMasterFragments.RoutePlan;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.test.View.Adapter.RoutePlanAdapter;
import com.example.test.Model.Customer;
import com.example.test.Model.RoutePlan;
import com.example.test.Model.User;
import com.example.test.Presenter.CustomerMasterPresenter;
import com.example.test.Presenter.RoutePlanMasterPresenter;
import com.example.test.View.interfaces.ViewCustomer;
import com.example.test.View.interfaces.ViewRoutePlan;
import com.example.test.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class AddRoutePlan extends Fragment implements  ViewRoutePlan,ViewCustomer {

    ListView listView;
    List<RoutePlan> dataRoute;
    List<Customer> dataCustomer;
    RoutePlanAdapter customAdapter;
    ArrayAdapter arrayAdapter;
    TextView userRoute;
    TextView dateRoute;
    Spinner spinner;
    User user;
    String date;
    CustomerMasterPresenter customerMasterPresenter;
    RoutePlanMasterPresenter routePlanMasterPresenter;
    FloatingActionButton floatingActionButton;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_assigned_customer, container, false);
        routePlanMasterPresenter = new RoutePlanMasterPresenter(this);
        customerMasterPresenter= new CustomerMasterPresenter(this);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
             user = new User();
             user.setId(bundle.getInt("User"));
             date = bundle.getString("Date");
            routePlanMasterPresenter.getRoutePlanByUserAndDate(user,date);
        }
        userRoute = view.findViewById(R.id.userListRoute);
        dateRoute = view.findViewById(R.id.dateListRoute);
        dateRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                DatePicker picker = new DatePicker(getContext());
                Calendar today = Calendar.getInstance();
                long now = today.getTimeInMillis();
                picker.setMinDate(now);
                builder.setView(picker);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setPositiveButton("Next", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int   day  = picker.getDayOfMonth();
                        int   month= picker.getMonth();
                        int   year = picker.getYear();

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, month, day);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        String formatedDate = sdf.format(calendar.getTime());
                        dateRoute.setText(formatedDate);
                        routePlanMasterPresenter.getRoutePlanByUserAndDate(user,formatedDate);
                        date = formatedDate;
                    }
                });
                builder.show();
            }
        });
        userRoute.setText(bundle.getString("UserName"));
        dateRoute.setText(date);
        listView = view.findViewById(R.id.listRoute);
        floatingActionButton = view.findViewById(R.id.floatingButtonAddListRoute);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogForm();
            }
        });
        return view;
    }
    private void DialogForm() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Select Customer");
        customerMasterPresenter.getCustomerData();
        spinner = new Spinner(getContext());
        dialog.setView(spinner);
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                customerMasterPresenter.getCustomerData();
                RoutePlan routePlan = new RoutePlan();
                routePlan.setUser(user);
                routePlan.setCustomer(dataCustomer.get(spinner.getSelectedItemPosition()));
                routePlan.setDate(date);
                routePlanMasterPresenter.saveRoutePlan(routePlan);
            }
        });
        dialog.setTitle("Add Customer");
        dialog.show();
    }


    @Override
    public void updateRoutePlanList(List RoutePlan) {
        customAdapter = new RoutePlanAdapter(getContext(),RoutePlan,routePlanMasterPresenter);
        listView.setAdapter(customAdapter);;
        dataRoute = RoutePlan;
        customAdapter.notifyDataSetChanged();
    }
    public List<User> getCustomerName(List<Customer> data){
        List temp = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            temp.add(data.get(i).getName());
        }
        return temp;
    }
    public List<Customer> getAllCustomer(List<RoutePlan> data){
        List temp = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            temp.add(data.get(i).getCustomer());
        }
        return temp;
    }

    @Override
    public void updateCustomerInfoListView(List customer) {
        arrayAdapter = new ArrayAdapter<>(getContext(),R.layout.support_simple_spinner_dropdown_item,getCustomerName(customer));
        spinner.setAdapter(arrayAdapter);
        dataCustomer = customer;
        customAdapter.notifyDataSetChanged();
    }
}
package com.example.test.View.BaseHomeNavBarFragments.ReportFragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.test.Model.SurveyAnswer;
import com.example.test.Model.User;
import com.example.test.Presenter.SurveyAnswerPresenter;
import com.example.test.Presenter.UserMasterPresenter;
import com.example.test.View.interfaces.ViewSurveyAnswer;
import com.example.test.View.interfaces.ViewUser;
import com.example.test.R;

import java.util.ArrayList;
import java.util.List;

public class SurveyReportList extends Fragment implements ViewSurveyAnswer, ViewUser {
    ListView listView;
    String sales;
    String date;
    List<SurveyAnswer> surveyList;
    UserMasterPresenter userMasterPresenter;
    SurveyAnswerPresenter surveyMasterPresenter;
    public SurveyReportList(){
        userMasterPresenter = new UserMasterPresenter(this);
        surveyMasterPresenter = new SurveyAnswerPresenter(this);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_survey_report_list, container, false);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            sales = bundle.getString("usernameSales");
            date = bundle.getString("date");
            userMasterPresenter.findById(Integer.parseInt(sales));
        }
        listView = view.findViewById(R.id.listUserSurveyReport);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity(),SurveyReportDetailsActivity.class);
                i.putExtra("usernameSales",sales);
                i.putExtra("customer",String.valueOf(surveyList.get(position).getCustomer().getId()));
                i.putExtra("date",date);
                startActivity(i);
            }
        });
        return view;
    }
    public List getSurvey(List<SurveyAnswer> data){
      List temp = new ArrayList();
        for (int i = 0; i < data.size(); i++) {
            temp.add(data.get(i).getCustomer().getName());
        }
        return temp;
    }


    @Override
    public void updateUserList(List<User> user) {
        surveyMasterPresenter.getCustomerByUser(String.valueOf(user.get(0).getId()),date);
    }

    @Override
    public void updateSurveyAnswerList(List<SurveyAnswer> survey) {
        listView.setAdapter(new ArrayAdapter<>(getContext(),R.layout.support_simple_spinner_dropdown_item,getSurvey(survey)));
        surveyList = survey;
    }
}
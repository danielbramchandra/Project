package com.example.test.View.BaseHomeNavBarFragments.ReportFragments;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.test.Model.SurveyAnswer;
import com.example.test.Model.User;
import com.example.test.Presenter.SurveyAnswerPresenter;
import com.example.test.Presenter.UserMasterPresenter;
import com.example.test.View.interfaces.ViewSurveyAnswer;
import com.example.test.View.interfaces.ViewUser;
import com.example.test.R;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class SurveyReportFragment extends Fragment implements ViewUser, ViewSurveyAnswer {
    ListView listView;
    ListView listDate;
    AlertDialog alert;
    List<User> userData;
    List<SurveyAnswer> surveyAnswersDate;
    UserMasterPresenter presenterUser;
    SurveyAnswerPresenter presenterSurvey;

    public SurveyReportFragment() {
        userData = new LinkedList<>();
        presenterUser = new UserMasterPresenter(this);
        presenterSurvey = new SurveyAnswerPresenter(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_survey_report, container, false);
        listView = view.findViewById(R.id.listSurveyReport);
        presenterUser.getUserHaveRoleSurvey();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                presenterSurvey.getDateByUser(String.valueOf(userData.get(position).getId()));
                listDate = new ListView(getContext());
                Bundle bundle = new Bundle();
                bundle.putString("usernameSales", String.valueOf(userData.get(position).getId()));
                dialog.setView(listDate);
                dialog.setTitle("Select Date");
                listDate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        SurveyReportList nextFrag = new SurveyReportList();
                        bundle.putString("date", surveyAnswersDate.get(position).getAnswerDate());
                        nextFrag.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_container, nextFrag)
                                .addToBackStack(null)
                                .commit();
                        alert.dismiss();
                    }
                });
                alert = dialog.create();
                alert.show();

            }
        });
        return view;
    }

    public List getName(List<User> data) {
        List temp = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            temp.add(data.get(i).getName());
        }
        return temp;
    }

    public List getDate(List<SurveyAnswer> data) {
        List temp = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            temp.add(data.get(i).getAnswerDate());
        }
        return temp;
    }

    @Override
    public void updateUserList(List<User> user) {
        listView.setAdapter(new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, getName(user)));
        userData = user;
    }

    @Override
    public void updateSurveyAnswerList(List<SurveyAnswer> survey) {
        listDate.setAdapter(new ArrayAdapter<>(getActivity(), R.layout.support_simple_spinner_dropdown_item, getDate(survey)));
        surveyAnswersDate = survey;
    }
}
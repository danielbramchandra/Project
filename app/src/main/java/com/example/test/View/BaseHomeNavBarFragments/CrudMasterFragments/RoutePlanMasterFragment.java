package com.example.test.View.BaseHomeNavBarFragments.CrudMasterFragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ListView;

import com.example.test.Model.User;
import com.example.test.Presenter.UserMasterPresenter;
import com.example.test.View.interfaces.ViewUser;
import com.example.test.R;
import com.example.test.View.BaseHomeNavBarFragments.CrudMasterFragments.RoutePlan.AddRoutePlan;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class RoutePlanMasterFragment extends Fragment implements ViewUser {
    ListView listView;
    UserMasterPresenter presenter;
    ArrayAdapter adapter;
    List<User> listData;

    public RoutePlanMasterFragment() {
        presenter = new UserMasterPresenter(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_route_plan_master, container, false);
        listView = view.findViewById(R.id.listUserRoutePlan);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                DatePicker picker = new DatePicker(getContext());
                Calendar today = Calendar.getInstance();
                long now = today.getTimeInMillis();
                picker.setMinDate(now);
                builder.setView(picker);
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).setPositiveButton("Next", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AddRoutePlan nextFrag = new AddRoutePlan();
                        int day = picker.getDayOfMonth();
                        int month = picker.getMonth();
                        int year = picker.getYear();

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(year, month, day);
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        String formatedDate = sdf.format(calendar.getTime());
                        Bundle bundle = new Bundle();
                        bundle.putString("Date", formatedDate);
                        bundle.putString("UserName", listData.get(position).getName());
                        bundle.putInt("User", listData.get(position).getId());
                        nextFrag.setArguments(bundle);
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.fl_container, nextFrag)
                                .addToBackStack(null)
                                .commit();
                    }
                });
                builder.show();
            }
        });
        presenter.getUserData();
        return view;
    }

    public List getUser(List<User> data) {
        List temp = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            temp.add(data.get(i).getName());
        }
        return temp;
    }

    @Override
    public void updateUserList(List<User> user) {
        adapter = new ArrayAdapter(getContext(), R.layout.support_simple_spinner_dropdown_item, getUser(user));
        listView.setAdapter(adapter);
        listData = user;
    }
}
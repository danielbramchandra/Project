package com.example.test.View.BaseHomeNavBarFragments.CrudMasterFragments.Survey;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.test.View.Adapter.SurveyAdapter;
import com.example.test.Model.Survey;
import com.example.test.Model.User;
import com.example.test.Presenter.SurveyMasterPresenter;
import com.example.test.View.interfaces.ViewSurvey;
import com.example.test.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class AddSurvey extends Fragment implements ViewSurvey {
    EditText question;
    Spinner type;
    User user;
    List surveyData;
    List listType;
    List listUser;
    ListView listView;
    SurveyAdapter customAdapter;
    FloatingActionButton save;
    SurveyMasterPresenter presenter;
    public AddSurvey(){
        presenter = new SurveyMasterPresenter(this);
        listType = new ArrayList();
    }
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_add_survey,container,false);
        listView = view.findViewById(R.id.listSurveyMaster);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
              user = new User();
             user.setId(bundle.getInt("User"));
            presenter.getSurveyDataFromUser(user);
        }
        save = view.findViewById(R.id.surveyMasterfloatingActionButton);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogForm();
            }
        });
        return view;
    }
    private void DialogForm() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.form_add_survey, null);
        LinearLayout rootLayout = dialogView.findViewById(R.id.rootView);
        listType.clear();
        listType.add("FreeText");
        listType.add("RadioButton");
        listType.add("CheckBox");
        question = dialogView.findViewById(R.id.addQuestion);
        type = dialogView.findViewById(R.id.typeOption);
        type.setAdapter(new ArrayAdapter<>(getContext(),R.layout.support_simple_spinner_dropdown_item,listType));
        type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!listType.get(position).toString().matches("FreeText")) {
                    rootLayout.removeAllViews();
                    Button add = new Button(getContext());
                    add.setText("Add");
                    TextView textView = new TextView(getContext());
                    textView.setText("Option");
                    EditText editText = new EditText(getContext());
                    rootLayout.addView(add);
                    rootLayout.addView(textView);
                    rootLayout.addView(editText);
                    add.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(rootLayout.getChildCount()<6){
                                EditText editText = new EditText(getContext());
                                rootLayout.addView(editText);
                            }
                        }
                    });

                }else{
                    rootLayout.removeAllViews();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        dialog.setView(dialogView);
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(question.getText().toString().equals("")){
                    Toast toast = Toast.makeText(getContext(),"Please Complete The Form",Toast.LENGTH_SHORT);
                    toast.show();
                }else{
                    Survey survey = new Survey();
                    ViewGroup viewGroup = (ViewGroup) rootLayout;
                    String temp = "";
                    for (int i = 0; i < viewGroup.getChildCount(); i++) {
                        View viewChild = viewGroup.getChildAt(i);
                        if(viewChild instanceof EditText){
                            temp+= ((EditText) viewChild).getText().toString();
                            if(viewGroup.getChildCount()-i !=1){
                                temp+= ";";
                            }
                        }

                    }

                    survey.setSurveyQuestion(question.getText().toString());
                    survey.setType(type.getSelectedItem().toString());
                    survey.setSurveyOption(temp);
                    survey.setUser(user);
                    presenter.saveSurveyData(survey);
                }
            }
        });
        dialog.setTitle("Add Survey");
        dialog.show();
    }

    @Override
    public void updateSurveyList(List survey) {
        customAdapter = new SurveyAdapter(getContext(),survey,presenter);
        listView.setAdapter(customAdapter);
        surveyData = survey;
    }
}

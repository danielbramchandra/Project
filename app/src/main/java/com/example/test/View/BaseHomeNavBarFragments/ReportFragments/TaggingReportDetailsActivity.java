package com.example.test.View.BaseHomeNavBarFragments.ReportFragments;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.Tagging;
import com.example.test.Presenter.TaggingMasterPresenter;
import com.example.test.View.interfaces.ViewTagging;
import com.example.test.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class TaggingReportDetailsActivity extends AppCompatActivity implements ViewTagging {
    Tagging tagging;
    ImageView imageView;
    String id;
    TextView contractNum;
    TextView serialNum;
    TextView taggingDate;
    TextView nonActiveAssetDate;
    TextView customer;
    TextView sales;
    TaggingMasterPresenter presenter;

    public TaggingReportDetailsActivity() {
        presenter = new TaggingMasterPresenter(this);
        tagging = new Tagging();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tagging_report_details);
        Intent i = getIntent();
        id = i.getStringExtra("Tagging");
        presenter.getTaggingById(id);

        imageView = findViewById(R.id.imageDetails);
        contractNum = findViewById(R.id.contractNumber);
        serialNum = findViewById(R.id.serialnumber);
        taggingDate = findViewById(R.id.taggingDate);
        nonActiveAssetDate = findViewById(R.id.nonActiveAsset);
        customer = findViewById(R.id.customerDetails);
        sales = findViewById(R.id.salesDetail);


    }

    @Override
    public void updateTaggingList(List<Tagging> taggingData) {
        System.out.println(taggingData.get(0).getUrlPhoto());
        tagging = taggingData.get(0);
        if (tagging.getUrlPhoto() != null) {
            Picasso.get().load(tagging.getUrlPhoto().replace("localhost", APIUtils.URL)).into(imageView);
        }
        serialNum.setText(String.valueOf(tagging.getSerialNumber()));
        contractNum.setText(String.valueOf(tagging.getNomerKontrak()));
        Date dateTagging = tagging.getTaggingDate();
        DateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        taggingDate.setText(simpleDateFormat.format(dateTagging));
        sales.setText(tagging.getUser().getName().toString());
        Date dateNonActive = tagging.getTglNonaktifAsset();
        nonActiveAssetDate.setText(simpleDateFormat.format(dateNonActive));
        customer.setText(tagging.getCustomer().getName());
    }
}
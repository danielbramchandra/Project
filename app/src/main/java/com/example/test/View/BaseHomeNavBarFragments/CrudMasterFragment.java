package com.example.test.View.BaseHomeNavBarFragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.example.test.R;
import com.example.test.View.BaseHomeNavBarFragments.CrudMasterFragments.CustomerMasterFragment;
import com.example.test.View.BaseHomeNavBarFragments.CrudMasterFragments.RoutePlanMasterFragment;
import com.example.test.View.BaseHomeNavBarFragments.CrudMasterFragments.SurveyMasterFragment;
import com.example.test.View.BaseHomeNavBarFragments.CrudMasterFragments.UserMasterFragment;

import org.jetbrains.annotations.NotNull;

public class CrudMasterFragment extends Fragment {
    CardView costumerMasterButton;
    CardView surveyMasterButton;
    CardView routePlanMasterButton;
    CardView userMasterButton;
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_crudmaster,container,false);
        costumerMasterButton = view.findViewById(R.id.cardCustomer);
        costumerMasterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomerMasterFragment nextFrag = new CustomerMasterFragment();
                loadFragment(nextFrag);
            }
        });
        surveyMasterButton = view.findViewById(R.id.cardSurvey);
        surveyMasterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SurveyMasterFragment nextFrag = new SurveyMasterFragment();
                loadFragment(nextFrag);
            }
        });
        routePlanMasterButton = view.findViewById(R.id.cardTagging);
        routePlanMasterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RoutePlanMasterFragment nextFrag = new RoutePlanMasterFragment();
                loadFragment(nextFrag);
            }
        });
        userMasterButton = view.findViewById(R.id.cardUser);
        userMasterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserMasterFragment nextFrag = new UserMasterFragment();
                loadFragment(nextFrag);
            }
        });

        return view;

    }
    private boolean loadFragment(Fragment fragment) {
        if(fragment!=null){
            getActivity().getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fl_container, fragment)
                    .addToBackStack(null)
                    .commit();
            return true;
        }
        return  false;
    }
}

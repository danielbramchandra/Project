package com.example.test.View.BaseHomeNavBarFragments.HomeFragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.test.Model.User;
import com.example.test.Presenter.UserMasterPresenter;
import com.example.test.View.interfaces.ViewUser;
import com.example.test.R;
import com.example.test.View.HomeActivity;

import java.util.List;

public class UbahPasswordFragment extends Fragment implements ViewUser {
    EditText currentPasswordField;
    EditText reTypePasswordField;
    EditText newPasswordText;
    User user;
    Button saveChangePasswordButton;
    UserMasterPresenter presenter;
    public UbahPasswordFragment(){
        presenter = new UserMasterPresenter(this);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ubah_password, container, false);
        SharedPreferences sharedPreferences = this.getContext().getSharedPreferences("UserLogin", Context.MODE_PRIVATE);
        String uname =  sharedPreferences.getString("username", "");
        presenter.findByUsername(uname);
        currentPasswordField =  view.findViewById(R.id.currentPasswordField);
        newPasswordText = view.findViewById(R.id.newPasswordField);
        reTypePasswordField= view.findViewById(R.id.reTypePasswordField);
        saveChangePasswordButton = view.findViewById(R.id.saveChangePasswordButton);
        saveChangePasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!currentPasswordField.getText().toString().equals(user.getPassword())){
                    Toast toast = Toast.makeText(getContext(),"Current Password Incorrect",Toast.LENGTH_SHORT);
                    toast.show();
                }else{
                    if(!newPasswordText.getText().toString().equals(reTypePasswordField.getText().toString())){
                        Toast toast = Toast.makeText(getContext(),"Re Type Password Incorrect",Toast.LENGTH_SHORT);
                        toast.show();
                    }else{
                        Toast toast = Toast.makeText(getContext(),"Change Password Success",Toast.LENGTH_SHORT);
                        toast.show();
                        user.setPassword(newPasswordText.getText().toString());
                        presenter.updateUser(user);
                        SharedPreferences sharedPreferences = getContext().getSharedPreferences("UserLogin", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("username",user.getUsername());
                        Intent intent = new Intent(getContext(), HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);

                    }
                }
            }
        });
        return view;
    }

    @Override
    public void updateUserList(List<User> user) {
        this.user = user.get(0);
    }
}
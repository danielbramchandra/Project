package com.example.test.View.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.example.test.Model.Customer;
import com.example.test.Model.RoutePlan;
import com.example.test.Presenter.CustomerMasterPresenter;
import com.example.test.Presenter.RoutePlanMasterPresenter;
import com.example.test.View.interfaces.ViewCustomer;
import com.example.test.View.interfaces.ViewRoutePlan;
import com.example.test.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class RoutePlanAdapter extends ArrayAdapter<String> implements ViewCustomer, ViewRoutePlan {
    private Context context;
    private List<RoutePlan> values;
    RoutePlanMasterPresenter presenterRoutePlan;
    CustomerMasterPresenter presenterCustomer;
    TextView textView;
    AlertDialog.Builder dialog;
    ImageButton deleteButton;
    ImageButton editButton;
    Spinner customerSpinner;

    public RoutePlanAdapter(@NonNull Context context, @NonNull List objects, RoutePlanMasterPresenter routePlanMasterPresenter) {
        super(context, R.layout.row_layout_crud, objects);
        this.context = context;
        this.values = objects;
        this.presenterCustomer = new CustomerMasterPresenter(this);
        this.presenterRoutePlan = routePlanMasterPresenter;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.row_layout_crud, parent, false);
        textView = view.findViewById(R.id.textRow);
        textView.setTextSize(15);
        textView.setTextColor(ContextCompat.getColor(context, R.color.black));
        deleteButton = view.findViewById(R.id.deleteRow);
        editButton = view.findViewById(R.id.editRow);
        displayText(values.get(position), inflater);
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editData(values.get(position), inflater);
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                dialog.setMessage("Delete Data?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteData(values.get(position));
                    }
                }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        return view;

    }

    public void displayText(RoutePlan value, LayoutInflater inflater) {
        textView.setText(value.getCustomer().getName());
    }

    public void deleteData(RoutePlan value) {
        presenterRoutePlan.deleteRoute(value);
    }

    public void editData(RoutePlan value, LayoutInflater inflater) {
        dialog = new AlertDialog.Builder(context);
        presenterCustomer.getCustomerData();
        customerSpinner = new Spinner(getContext());
        String str = value.getDate().toString();
        System.out.println(str);
        String[] arrStr = str.split("-");
        int mYear = Integer.parseInt(arrStr[0]);
        int mMonth = Integer.parseInt(arrStr[1]) - 1;
        int mDay = Integer.parseInt(arrStr[2]);
        DatePicker datePicker = new DatePicker(getContext());
        Calendar today = Calendar.getInstance();
        long now = today.getTimeInMillis();
        datePicker.setMinDate(now);
        datePicker.init(mYear, mMonth, mDay, null);
        dialog.setView(datePicker);
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                int day = datePicker.getDayOfMonth();
                int month = datePicker.getMonth();
                int year = datePicker.getYear();
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, month, day);
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                String formatedDate = sdf.format(calendar.getTime());
                String dateBefore = value.getDate().toString();
                value.setDate(formatedDate);
                presenterRoutePlan.updateRoute(value, dateBefore);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public List<Customer> getUsersName(List<Customer> data) {
        List temp = new ArrayList<>();
        for (int i = 0; i < data.size(); i++) {
            temp.add(data.get(i).getName());
        }
        return temp;
    }

    @Override
    public void updateCustomerInfoListView(List customer) {
        customerSpinner.setAdapter(new ArrayAdapter<>(getContext(), R.layout.support_simple_spinner_dropdown_item, getUsersName(customer)));
    }

    @Override
    public void updateRoutePlanList(List RoutePlan) {
    }
}

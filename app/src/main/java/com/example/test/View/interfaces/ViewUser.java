package com.example.test.View.interfaces;

import com.example.test.Model.User;

import java.util.List;

public interface ViewUser {
    void updateUserList(List<User> user);
}


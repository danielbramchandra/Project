package com.example.test.View.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.User;
import com.example.test.R;
import com.squareup.picasso.Picasso;

public class DialogUser {
    String[] genderArray = {"Male","Female"};
    String[] roleArray = {"Survey","Tagging"};
    AlertDialog.Builder dialog;
    View dialogView;
    EditText name;
    EditText username;
    EditText password;
    EditText phoneNumber;
    EditText email;
    TextView profilePic;
    ImageView picture;
    Button browseProfilePic;
    Spinner gender;
    Spinner role;
    DialogUser(Context context, LayoutInflater layoutInflater){
        dialog = new AlertDialog.Builder(context);
        dialogView = layoutInflater.inflate(R.layout.form_data_user, null);
        picture = dialogView.findViewById(R.id.profilePicture);
        profilePic = dialogView.findViewById(R.id.profilePictureText);
        browseProfilePic = dialogView.findViewById(R.id.browseImageButton);
         name = dialogView.findViewById(R.id.userMasterNameField);
         username = dialogView.findViewById(R.id.userMasterUsernameField);
         password = dialogView.findViewById(R.id.userMasterPasswordField);
         phoneNumber = dialogView.findViewById(R.id.userMasterPhoneNumberField);
         email = dialogView.findViewById(R.id.userMasterEmailField);
         gender = dialogView.findViewById(R.id.userMasterGenderSpinner);
        gender.setAdapter(new ArrayAdapter<>(context,R.layout.support_simple_spinner_dropdown_item,genderArray));
         role = dialogView.findViewById(R.id.userMasterRoleSpinner);
        role.setAdapter(new ArrayAdapter<>(context,R.layout.support_simple_spinner_dropdown_item,roleArray));
        dialog.setTitle("Details Sales");
        dialog.setView(dialogView);
    }
    public void setData(User user){
        gender.setSelection(getIndex(gender,user.getGender()));
        role.setSelection(getIndex(role,user.getRole()));
        name.setText(user.getName());
        username.setText(user.getUsername());
        password.setText(user.getPassword());
        if(user.getUrlPhoto()!=null){
            Picasso.get().load(user.getUrlPhoto().replace("localhost", APIUtils.URL)).into(picture);
        }
        phoneNumber.setText(user.getPhoneNumber());
        email.setText(user.getEmail());
    }
    public void setNotClickable(){
        gender.setFocusable(false);
        role.setFocusable(false);
        name.setFocusable(false);
        username.setFocusable(false);
        password.setFocusable(false);
        phoneNumber.setFocusable(false);
        email.setFocusable(false);
        gender.setBackground(null);
        role.setBackground(null);
        name.setBackground(null);
        username.setBackground(null);
        password.setBackground(null);
        phoneNumber.setBackground(null);
        email.setBackground(null);
    }
    public int getIndex(Spinner spinner, String s){
        for (int i = 0; i < spinner.getCount(); i++) {
            if(spinner.getItemAtPosition(i).toString().equalsIgnoreCase(s)){
                return i;
            }
        }
        return 0;
    }
    public EditText getEditTextName() {
        return name;
    }

    public EditText getEditTextUsername() {
        return username;
    }

    public EditText getEditTextPassword() {
        return password;
    }

    public EditText getEditTextPhoneNumber() {
        return phoneNumber;
    }

    public EditText getEditTextEmail() {
        return email;
    }

    public Spinner getEditTextGender() {
        return gender;
    }

    public Spinner getEditTextRole() {
        return role;
    }

    public ImageView getPicture() {
        return picture;
    }

    public TextView getProfilePic() {
        return profilePic;
    }

    public Button getBrowseProfilePic() {
        return browseProfilePic;
    }

    public AlertDialog.Builder getDialog() {
        return dialog;
    }

    public  void show(){
        dialog.show();
    }
}

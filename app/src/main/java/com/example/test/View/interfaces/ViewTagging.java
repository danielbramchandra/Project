package com.example.test.View.interfaces;

import com.example.test.Model.Tagging;

import java.util.List;

public interface ViewTagging {
    void updateTaggingList(List<Tagging> tagging);
}

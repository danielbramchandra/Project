package com.example.test.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.example.test.Presenter.MainActivityPresenter;
import com.example.test.R;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.safetynet.SafetyNet;
import com.google.android.gms.safetynet.SafetyNetApi;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;


public class MainActivity extends AppCompatActivity {
    private MainActivityPresenter presenter;
    public String TAG = "Main Activity";
    String siteKey = "6Lfz61IbAAAAAHqDD7ltTqJf1JDzHM2tNuG2UXqg";
//    String secretKey = "6Lfz61IbAAAAAOGFf9HJbWpDpalyj7Yq4zhzjLFj";
    public String userResponseToken;
    Button signin;
    EditText username;
    EditText pass;
    @Override
    protected void onResume() {
        super.onResume();
        String credential = getCredential();
        if(!credential.isEmpty()){
            Intent intent = new Intent(MainActivity.this,HomeActivity.class);
            startActivity(intent);
        }
    }
    public String getCredential(){
        SharedPreferences sharedPreferences = getSharedPreferences("UserLogin", Context.MODE_PRIVATE);
        return  sharedPreferences.getString("username", "");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String credential = getCredential();
        System.out.println(credential);
        if(!credential.matches("")){
            Intent intent = new Intent(MainActivity.this,HomeActivity.class);
            startActivity(intent);
        }
        presenter = new MainActivityPresenter();
        signin = findViewById(R.id.signinbutton);

         username = findViewById(R.id.uname);
         pass = findViewById(R.id.password);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connect(v);

            }
        });

    }
    public void connect(View v){
        SafetyNet.getClient(this).verifyWithRecaptcha(siteKey).addOnSuccessListener( this, new OnSuccessListener<SafetyNetApi.RecaptchaTokenResponse>() {
            @Override
            public void onSuccess(SafetyNetApi.RecaptchaTokenResponse recaptchaTokenResponse) {
                 userResponseToken = recaptchaTokenResponse.getTokenResult();
                if(!userResponseToken.isEmpty()){
                    String name = username.getText().toString();
                    String passw = pass.getText().toString();
                    presenter.login(name,passw,getApplicationContext());
                }
            }
        }).addOnFailureListener( this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
           
            }
        });
    }

}


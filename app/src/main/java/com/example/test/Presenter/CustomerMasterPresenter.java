package com.example.test.Presenter;

import com.example.test.Model.Controller.CustomerRepo;
import com.example.test.Model.Controller.Interface.UpdateListCustomer;
import com.example.test.Model.Customer;
import com.example.test.Model.Repo.APIService;
import com.example.test.Model.Repo.APIUtils;
import com.example.test.View.interfaces.ViewCustomer;

import java.util.LinkedList;
import java.util.List;

public class CustomerMasterPresenter implements UpdateListCustomer {
    List data;
    ViewCustomer view;
    APIService mApiService;
    CustomerRepo repo;
    public CustomerMasterPresenter(ViewCustomer view) {
        this.view = view;
        data = new LinkedList();
        mApiService = APIUtils.getAPIService();
        repo = new CustomerRepo(this);
    }


    public void saveCustomerData(Customer customer) {
        repo.saveCustomer(customer);
    }
    public void deleteCustomer(int userId){
        repo.deleteCustomer(userId);
    }
    public void getCustomerData(){
        repo.getAllCustomer();

    }
    @Override
    public void UpdateList(List list) {
        view.updateCustomerInfoListView(list);
        data = list;
    }
}

package com.example.test.Presenter;

import com.example.test.Model.Controller.Interface.UpdateListRoutePlan;
import com.example.test.Model.Controller.RoutePlanRepo;
import com.example.test.Model.Repo.APIService;
import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.RoutePlan;
import com.example.test.Model.User;
import com.example.test.View.interfaces.ViewRoutePlan;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoutePlanMasterPresenter implements UpdateListRoutePlan {
    APIService apiService;
    List data;
    ViewRoutePlan viewRoutePlan;
    RoutePlanRepo routePlanRepo;
    public RoutePlanMasterPresenter(ViewRoutePlan viewRoutePlan){
        apiService = APIUtils.getAPIService();
        data = new ArrayList();
        this.viewRoutePlan = viewRoutePlan;
        routePlanRepo = new RoutePlanRepo(this);
    }
    public void saveRoutePlan(RoutePlan routePlan){
        routePlanRepo.saveRoutePlan(routePlan);
    }
    public  void getRouteByUser(User user){
        routePlanRepo.getRouteByUser(user);
    }


    public  void getRoutePlanByUserAndDate(User user,String str){
        routePlanRepo.getRoutePlanByUserAndDate(user,str);
    }
    public void deleteRoute(RoutePlan routePlan){
        routePlanRepo.deleteRoute(routePlan);
    }
    public  void updateRoute(RoutePlan routePlan, String date){
        routePlanRepo.updateRoute(routePlan,date);
    }

    @Override
    public void UpdateList(List<RoutePlan> list) {
        viewRoutePlan.updateRoutePlanList(list);
    }
}

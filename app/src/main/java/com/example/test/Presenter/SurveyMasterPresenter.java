package com.example.test.Presenter;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.test.Model.Controller.Interface.UpdateListSurvey;
import com.example.test.Model.Controller.SurveyRepo;
import com.example.test.Model.Repo.APIService;
import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.Survey;
import com.example.test.Model.User;
import com.example.test.View.interfaces.ViewSurvey;

import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveyMasterPresenter implements UpdateListSurvey {
    ViewSurvey viewSurvey;
    SurveyRepo surveyRepo;
    public SurveyMasterPresenter(ViewSurvey viewSurvey){
        this.viewSurvey = viewSurvey;
        surveyRepo = new SurveyRepo(this);
    }

    public  void saveSurveyData(Survey survey){
        surveyRepo.saveSurveyData(survey);
    }
    public  void  deleteSurveyData(Survey survey){
        surveyRepo.deleteSurveyData(survey);
    }

    public void getSurveyDataFromUser(User user){
        surveyRepo.getSurveyDataFromUser(user);

    }

    @Override
    public void UpdateList(List<Survey> list) {
        viewSurvey.updateSurveyList(list);
    }
}

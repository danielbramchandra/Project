package com.example.test.Presenter;

import com.example.test.Model.Controller.Interface.UpdateListSurveyAnswer;
import com.example.test.Model.Controller.SurveyAnswerRepo;
import com.example.test.Model.Repo.APIService;
import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.SurveyAnswer;
import com.example.test.View.interfaces.ViewSurveyAnswer;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveyAnswerPresenter implements UpdateListSurveyAnswer {
    APIService mApiservice;
    ViewSurveyAnswer viewSurveyAnswer;
    SurveyAnswerRepo repo;
    public SurveyAnswerPresenter(ViewSurveyAnswer viewSurveyAnswer){
        mApiservice = APIUtils.getAPIService();
        this.viewSurveyAnswer = viewSurveyAnswer;
        repo=new SurveyAnswerRepo(this);
    }
    public void getDateByUser(String user){
        repo.getDateByUser(user);
    }
    public void getCustomerByUser(String user,String date){
        repo.getCustomerByUser(user, date);

    }
    public void getAnswerByCustomerAndUser(String user, String customer,String date){
        repo.getAnswerByCustomerAndUser(user,customer,date);
    }

    @Override
    public void UpdateList(List<SurveyAnswer> surveyAnswerList) {
        viewSurveyAnswer.updateSurveyAnswerList(surveyAnswerList);
    }
}

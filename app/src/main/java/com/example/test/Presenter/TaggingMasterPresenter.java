package com.example.test.Presenter;

import android.app.ProgressDialog;

import com.example.test.Model.Controller.Interface.UpdateListTagging;
import com.example.test.Model.Controller.TaggingRepo;
import com.example.test.Model.Repo.APIService;
import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.Tagging;
import com.example.test.Model.User;
import com.example.test.View.interfaces.ViewTagging;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaggingMasterPresenter implements UpdateListTagging {
    List<Tagging> data;
    ViewTagging viewTagging;
    TaggingRepo repo;
    public TaggingMasterPresenter(ViewTagging viewTagging){
        data = new LinkedList<>();
        this.viewTagging = viewTagging;
        repo = new TaggingRepo(this);
    }
    public void getTaggingById(String id) {
        repo.getTaggingById(id);
    }
    public void getTaggingDataByUser(User user){
        repo.getTaggingDataByUser(user);
    }

    @Override
    public void UpdateList(List<Tagging> list) {
        viewTagging.updateTaggingList(list);
    }
}

package com.example.test.Presenter;


import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.example.test.Model.Controller.Interface.UpdateListUser;
import com.example.test.Model.Controller.UserRepo;
import com.example.test.Model.Repo.APIService;
import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.User;
import com.example.test.View.interfaces.ViewUser;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserMasterPresenter implements UpdateListUser {
    ViewUser view;
    List data;
    APIService apiService;
    UserRepo repo;
    public UserMasterPresenter(ViewUser view) {
        this.view = view;
        data = new LinkedList();
        apiService = APIUtils.getAPIService();
        repo = new UserRepo(this);
    }

    public void deleteUser(int userId){
        repo.deleteUser(userId);


    }
    public  void findByUsername(String uname){
        System.out.println(uname);
        repo.findByUsername(uname);

    }
    public void findById(int id){
        repo.findById(id);

    }
    public void saveUserDataWithPicture(User user, String imagePath){
        repo.saveUserDataWithPicture(user,imagePath);

    }
    public void saveUserData(User user, Context context){
        repo.saveUserData(user,context);

    }
    public void updateUser(User user){
        repo.updateUser(user);
    }
    public void getUserData(){
        repo.getUserData();

    }
    public void getUserHaveRoleTagging(){
        repo.getUserHaveRoleTagging();

    }
    public void getUserHaveRoleSurvey(){
        repo.getUserHaveRoleSurvey();

    }

    @Override
    public void UpdateList(List<User> list) {
        view.updateUserList(list);
    }
}

package com.example.test.Presenter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.example.test.Model.Controller.Interface.UpdateListUser;
import com.example.test.Model.Controller.UserRepo;
import com.example.test.Model.Repo.APIService;
import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.User;
import com.example.test.View.HomeActivity;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivityPresenter implements UpdateListUser {
    APIService apiService;
    UserRepo userRepo;
    public MainActivityPresenter(){
        apiService = APIUtils.getAPIService();
        userRepo =new UserRepo(this);
    }
    public void login(String username,String pass,Context context){
        userRepo.login(username,pass,context);
    }
    public static void saveToPreference(User user, Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserLogin", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("username",user.getUsername());
        editor.putString("password",user.getPassword());
        editor.commit();
    }

    @Override
    public void UpdateList(List<User> list) {

    }
}

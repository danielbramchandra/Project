package com.example.test.Model.Controller.Interface;

import com.example.test.Model.Survey;

import java.util.List;

public interface UpdateListSurvey {
    void UpdateList(List<Survey> list);
}

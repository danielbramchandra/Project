package com.example.test.Model;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.sql.Date;
import java.time.LocalDate;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RoutePlan {
    private Integer id;
    private User user;
    private Customer customer;
    private String date;
    private Boolean status;
    private Boolean deleted;

    /**
     * No args constructor for use in serialization
     *
     */
    public RoutePlan() {
    }

    /**
     *
     * @param date
     * @param deleted
     * @param id
     * @param user
     * @param customer
     * @param status
     */
    public RoutePlan(Integer id, User user, Customer customer, String date, Boolean status, Boolean deleted) {
        super();
        this.id = id;
        this.user = user;
        this.customer = customer;
        this.date = date;
        this.status = status;
        this.deleted = deleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }


//    @Override
//    public int describeContents() {
//        return 0;
//    }
//
//    @Override
//    public void writeToParcel(Parcel dest, int flags) {
//        dest.writeInt(id);
//        dest.writeParcelable(user, flags);
//        dest.writeByte((byte) (status ? 1 : 0));
//    }
}

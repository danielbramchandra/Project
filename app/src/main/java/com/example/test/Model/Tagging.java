package com.example.test.Model;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.Date;

public class Tagging implements Serializable {
    int id;
    String serialNumber;
    String urlPhoto;
    int nomerKontrak;
    Date tglNonaktifAsset;
    Date taggingDate;
    boolean deleted;
    Customer customer;
    User user;
    public Tagging() {
    }
    public Tagging(int id, String serialNumber, String urlPhoto, int nomerKontrak, Date tglNonaktifAsset,
                   Date taggingDate, boolean deleted, Customer customer,User user) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.urlPhoto = urlPhoto;
        this.nomerKontrak = nomerKontrak;
        this.tglNonaktifAsset = tglNonaktifAsset;
        this.taggingDate = taggingDate;
        this.deleted = deleted;
        this.customer = customer;
        this.user = user;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getSerialNumber() {
        return serialNumber;
    }
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public String getUrlPhoto() {
        return urlPhoto;
    }
    public void setUrlPhoto(String urlPhoto) {
        this.urlPhoto = urlPhoto;
    }
    public int getNomerKontrak() {
        return nomerKontrak;
    }
    public void setNomerKontrak(int nomerKontrak) {
        this.nomerKontrak = nomerKontrak;
    }
    public Date getTglNonaktifAsset() {
        return tglNonaktifAsset;
    }
    public void setTglNonaktifAsset(Date tglNonaktifAsset) {
        this.tglNonaktifAsset = tglNonaktifAsset;
    }
    public Date getTaggingDate() {
        return taggingDate;
    }
    public void setTaggingDate(Date taggingDate) {
        this.taggingDate = taggingDate;
    }
    public boolean isDeleted() {
        return deleted;
    }
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
    public Customer getCustomer() {
        return customer;
    }
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

}

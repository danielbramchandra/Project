package com.example.test.Model.Controller;

import android.app.ProgressDialog;

import com.example.test.Model.Controller.Interface.UpdateListTagging;
import com.example.test.Model.Repo.APIService;
import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.Tagging;
import com.example.test.Model.User;
import com.example.test.View.interfaces.ViewTagging;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaggingRepo {
    List<Tagging> data;
    UpdateListTagging updateListTagging;
    APIService apiService;
    public TaggingRepo(UpdateListTagging updateListTagging){
        apiService = APIUtils.getAPIService();
        data = new LinkedList<>();
        this.updateListTagging = updateListTagging;
    }
    public void getTaggingById(String id) {
        apiService.getTaggingById(id).enqueue(new Callback<Tagging>() {
            @Override
            public void onResponse(Call<Tagging> call, Response<Tagging> response) {
                List<Tagging> temp = new ArrayList();
                temp.add(response.body());
                updateListTagging.UpdateList(temp);
            }

            @Override
            public void onFailure(Call<Tagging> call, Throwable t) {
            }
        });
    }
    public void getTaggingDataByUser(User user){
        apiService.getAllTaggingByUser(user).enqueue(new Callback<List<Tagging>>() {
            @Override
            public void onResponse(Call<List<Tagging>> call, Response<List<Tagging>> response) {
                if(!response.body().isEmpty()){
                    updateListTagging.UpdateList(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Tagging>> call, Throwable t) {

            }
        });
    }
    public void getTaggingData(){
        apiService.getAllTagging().enqueue(new Callback<List<Tagging>>() {
            @Override
            public void onResponse(Call<List<Tagging>> call, Response<List<Tagging>> response) {
                if(!response.body().isEmpty()){
                    updateListTagging.UpdateList(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Tagging>> call, Throwable t) {
            }
        });
    }
}

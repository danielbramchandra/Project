package com.example.test.Model.Controller;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.example.test.Model.Controller.Interface.UpdateListCustomer;
import com.example.test.Model.Controller.Interface.UpdateListUser;
import com.example.test.Model.Repo.APIService;
import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.User;
import com.example.test.Presenter.MainActivityPresenter;
import com.example.test.View.HomeActivity;
import com.example.test.View.interfaces.ViewUser;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRepo  {

    List data;
    APIService apiService;
    UpdateListUser callback;
    public UserRepo(UpdateListUser callback) {

        data = new LinkedList();
        apiService = APIUtils.getAPIService();
        this.callback = callback;
    }

    public void deleteUser(int userId){
        apiService.deleteUser(userId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                data.clear();
                getUserData();
            }
            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });

    }
    public void updateUser(User user){
        apiService.updateUser(user).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }
    public  void findByUsername(String uname){
        apiService.findByUsername(uname).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                List<User> temp = new ArrayList<>();
                temp.add(response.body());
                callback.UpdateList(temp);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }
    public void findById(int id){
        apiService.findById(id).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                data.clear();
                data.add(response.body());
                callback.UpdateList(data);
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }


    public void saveUserDataWithPicture(User user, String imagePath){
        Gson gson = new Gson();
        File file = new File(imagePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json"),gson.toJson(user));
        RequestBody requestImage = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part part = MultipartBody.Part.createFormData("image",file.getName(), requestImage);
        apiService.saveWithPicture(requestBody,part).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                data.clear();
                getUserData();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }
    public void saveUserData(User user, Context context){
        apiService.saveUser(user).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Toast toast = null;
                if(response.body().equals("success")){
                    toast = Toast.makeText(context,"Registered Success",Toast.LENGTH_SHORT);
                    getUserData();
                }else if(response.body().equals("username exist")){
                     toast = Toast.makeText(context,"Username Already Registered",Toast.LENGTH_SHORT);
                }else if(response.body().equals("email exist")){
                     toast = Toast.makeText(context,"Email Already Registered",Toast.LENGTH_SHORT);
                }else{
                     toast = Toast.makeText(context,"Username and Email Already Registered",Toast.LENGTH_SHORT);
                }
                toast.show();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
            }
        });
    }
    public void getUserData(){
        apiService.getAllUser().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                data.clear();
                data.addAll(response.body());
                callback.UpdateList(data);
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

            }
        });
    }
    public void login(String username,String pass,Context context){
        User user = new User();
        user.setUsername(username);
        user.setPassword(pass);
        apiService.getAuth(user).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(response.body()!=null){
                    Toast toast = Toast.makeText(context,"Welcome "+response.body().getName(),Toast.LENGTH_SHORT);
                    toast.show();
                    MainActivityPresenter.saveToPreference(user,context);
                    Intent intent = new Intent(context, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Toast toast = Toast.makeText(context,"Username/Password Salah",Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }
    public void getUserHaveRoleTagging(){
        apiService.getAllUser().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                data.clear();
                List<User> temp =response.body();
                for (User user: temp) {
                    if(user.getRole().equalsIgnoreCase("Tagging")){
                        data.add(user);
                        callback.UpdateList(data);
                    }

                }

                callback.UpdateList(data);
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

            }
        });
    }
    public void getUserHaveRoleSurvey(){
        apiService.getAllUser().enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                data.clear();
                List<User> temp =response.body();
                for (User user: temp) {
                    if(user.getRole().equalsIgnoreCase("Survey")){
                        data.add(user);
                    }
                }

                callback.UpdateList(data);
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {

            }
        });
    }
}

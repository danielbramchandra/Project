package com.example.test.Model.Controller.Interface;

import com.example.test.Model.SurveyAnswer;

import java.util.List;

public interface UpdateListSurveyAnswer {
    void UpdateList(List<SurveyAnswer> surveyAnswerList);
}

package com.example.test.Model.Controller;


import com.example.test.Model.Controller.Interface.UpdateListCustomer;
import com.example.test.Model.Customer;
import com.example.test.Model.Repo.APIService;
import com.example.test.Model.Repo.APIUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerRepo {
    APIService mApiService;
    List data;
    UpdateListCustomer callback;
    public CustomerRepo(UpdateListCustomer callback) {
        mApiService = APIUtils.getAPIService();
        this.callback = callback;
    }
    public void getAllCustomer(){
        mApiService.getAllCustomer().enqueue(new Callback<List<Customer>>() {
            @Override
            public void onResponse(Call<List<Customer>> call, Response<List<Customer>> response) {
                callback.UpdateList(response.body());
            }

            @Override
            public void onFailure(Call<List<Customer>> call, Throwable t) {
            }
        });
    }
    public  List saveCustomer(Customer customer){
        mApiService.saveCustomer(customer).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                getAllCustomer();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
        return data;
    }
    public void  deleteCustomer(int userId){
        mApiService.deleteCustomer(userId).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                getAllCustomer();
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
            }
        });
    }
}

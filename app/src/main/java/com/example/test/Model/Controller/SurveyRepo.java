package com.example.test.Model.Controller;

import android.app.ProgressDialog;
import android.content.Context;

import com.example.test.Model.Controller.Interface.UpdateListCustomer;
import com.example.test.Model.Controller.Interface.UpdateListSurvey;
import com.example.test.Model.Repo.APIService;
import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.Survey;
import com.example.test.Model.User;
import com.example.test.View.interfaces.ViewSurvey;

import java.util.LinkedList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveyRepo {
    List data;
    ViewSurvey viewSurvey;
    APIService mApiservice;
    UpdateListSurvey callback;
    public SurveyRepo(UpdateListSurvey callback){
        this.callback = callback;
        data = new LinkedList();
        mApiservice = APIUtils.getAPIService();
    }

    public  void saveSurveyData(Survey survey){
        mApiservice.saveSurvey(survey).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                getSurveyDataFromUser(survey.getUser());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
    public  void  deleteSurveyData(Survey survey){
        mApiservice.deleteSurvey(survey.getId()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                data.clear();
                getSurveyDataFromUser(survey.getUser());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
    public  void updateSurvey(Survey survey){
        mApiservice.updateSurvey(survey).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                data.clear();
                getSurveyDataFromUser(survey.getUser());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
    public void getSurveyData(){
        mApiservice.getAllSurvey().enqueue(new Callback<List<Survey>>() {
            @Override
            public void onResponse(Call<List<Survey>> call, Response<List<Survey>> response) {
//                data.clear();
//                data.addAll(response.body());
//                viewSurvey.updateSurveyList(data);
                callback.UpdateList(response.body());
            }

            @Override
            public void onFailure(Call<List<Survey>> call, Throwable t) {

            }
        });
    }
    public void getSurveyDataFromUser(User user){
        mApiservice.getAllSurveyWithUser(user).enqueue(new Callback<List<Survey>>() {
            @Override
            public void onResponse(Call<List<Survey>> call, Response<List<Survey>> response) {
                callback.UpdateList(response.body());
            }

            @Override
            public void onFailure(Call<List<Survey>> call, Throwable t) {

            }
        });
    }
}

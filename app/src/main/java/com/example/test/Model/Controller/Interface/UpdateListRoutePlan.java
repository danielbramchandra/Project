package com.example.test.Model.Controller.Interface;

import com.example.test.Model.RoutePlan;

import java.util.List;

public interface UpdateListRoutePlan {
    void UpdateList(List<RoutePlan> list);
}

package com.example.test.Model;


import java.io.Serializable;
import java.util.Date;

public class Survey {
    int id;
    String surveyQuestion;
    String type;
    String surveyOption;
    Date surveyDate;
    User user;
    boolean deleted = false;
    public Survey() {
    }

    public Survey(int id, String surveyQuestion, String type, String surveyOption, Date surveyDate,
                  User user, boolean deleted) {
        this.id = id;
        this.surveyQuestion = surveyQuestion;
        this.type = type;
        this.surveyOption = surveyOption;
        this.surveyDate = surveyDate;
        this.user = user;
        this.deleted = deleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurveyQuestion() {
        return surveyQuestion;
    }

    public void setSurveyQuestion(String surveyQuestion) {
        this.surveyQuestion = surveyQuestion;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSurveyOption() {
        return surveyOption;
    }

    public void setSurveyOption(String surveyOption) {
        this.surveyOption = surveyOption;
    }

    public Date getSurveyDate() {
        return surveyDate;
    }

    public void setSurveyDate(Date surveyDate) {
        this.surveyDate = surveyDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }



}

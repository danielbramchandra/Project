package com.example.test.Model;

import java.time.LocalDate;

public class SurveyAnswer {
    int id;
    Survey survey;
    String answer;
    Customer customer;
    String answerDate;
    public SurveyAnswer(int id, Survey survey, String answer, Customer customer,String answerDate) {
        this.id = id;
        this.survey = survey;
        this.answer = answer;
        this.customer = customer;
        this.answerDate=answerDate;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public String getAnswer() {
        return answer;
    }

    public String getAnswerDate() {
        return answerDate;
    }

    public void setAnswerDate(String answerDate) {
        this.answerDate = answerDate;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

}

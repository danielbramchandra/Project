package com.example.test.Model.Controller;

import com.example.test.Model.Controller.Interface.UpdateListSurveyAnswer;
import com.example.test.Model.Controller.Interface.UpdateListTagging;
import com.example.test.Model.Repo.APIService;
import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.SurveyAnswer;
import com.example.test.View.interfaces.ViewSurveyAnswer;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveyAnswerRepo {
    APIService mApiservice;
    ViewSurveyAnswer viewSurveyAnswer;
    UpdateListSurveyAnswer updateListSurveyAnswer;
    public SurveyAnswerRepo(UpdateListSurveyAnswer updateListSurveyAnswer){
        mApiservice = APIUtils.getAPIService();
        this.updateListSurveyAnswer = updateListSurveyAnswer;
    }
    public void getDateByUser(String user){
        mApiservice.getDateByUser(user).enqueue(new Callback<List<SurveyAnswer>>() {
            @Override
            public void onResponse(Call<List<SurveyAnswer>> call, Response<List<SurveyAnswer>> response) {
                updateListSurveyAnswer.UpdateList(response.body());
            }

            @Override
            public void onFailure(Call<List<SurveyAnswer>> call, Throwable t) {
            }
        });
    }
    public void getCustomerByUser(String user,String date){
        mApiservice.getCustomerByUser(user,date).enqueue(new Callback<List<SurveyAnswer>>() {
            @Override
            public void onResponse(Call<List<SurveyAnswer>> call, Response<List<SurveyAnswer>> response) {
                updateListSurveyAnswer.UpdateList(response.body());
            }

            @Override
            public void onFailure(Call<List<SurveyAnswer>> call, Throwable t) {

            }
        });
    }
    public void getAnswerByCustomerAndUser(String user, String customer,String date){
        mApiservice.getAnswerByCusomerAndUser(user,customer,date).enqueue(new Callback<List<SurveyAnswer>>() {
            @Override
            public void onResponse(Call<List<SurveyAnswer>> call, Response<List<SurveyAnswer>> response) {
                updateListSurveyAnswer.UpdateList(response.body());
            }

            @Override
            public void onFailure(Call<List<SurveyAnswer>> call, Throwable t) {

            }
        });
    }
}

package com.example.test.Model.Repo;

import com.example.test.Model.Customer;
import com.example.test.Model.RoutePlan;
import com.example.test.Model.Survey;
import com.example.test.Model.SurveyAnswer;
import com.example.test.Model.Tagging;
import com.example.test.Model.User;

import java.util.List;
import java.util.Set;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APIService {

    @POST("/user/save")
    Call<String> saveUser(@Body User user);
    @GET("/user/delete/{id}")
    Call<Void> deleteUser(@Path("id") int id);
    @GET("/user")
    Call<List<User>> getAllUser();
    @POST("/user/auth")
    Call<User> getAuth(@Body User user);
    @PUT("/user/update/")
    Call<Void> updateUser(@Body User user);
    @GET("/user/uname?")
    Call<User> findByUsername(@Query("uname") String uname);
    @GET("/user/id?")
    Call<User> findById(@Query("id") int id);

    @POST("/survey/save")
    Call<Void> saveSurvey(@Body Survey survey);
    @GET("/survey/delete/{id}")
    Call<Void> deleteSurvey(@Path("id") int id);
    @GET("/survey")
    Call<List<Survey>> getAllSurvey();
    @POST("/survey/user")
    Call<List<Survey>> getAllSurveyWithUser(@Body User user);
    @PUT("/survey/update")
    Call<Void> updateSurvey(@Body Survey survey);

    @POST("/customer/save")
    Call<Void> saveCustomer(@Body Customer customer);
    @GET("/customer/delete/{id}")
    Call<Void> deleteCustomer(@Path("id") int id);
    @GET("/customer")
    Call<List<Customer>> getAllCustomer();
    @POST("/customer/update")
    Call<Void> updateCustomer(@Body Customer customer);

    @GET("/tagging/findbyid?")
    Call<Tagging> getTaggingById(@Query("id") String id);
    @POST("/tagging/save")
    Call<Tagging> saveTagging(@Body Tagging tagging);
    @GET("/tagging")
    Call<List<Tagging>> getAllTagging();
    @POST("/tagging/user")
    Call<List<Tagging>> getAllTaggingByUser(@Body User user);

    @POST("/routeplan/save")
    Call<Void> saveRoutePlan(@Body RoutePlan routePlan);
    @POST("/routeplan/getByUser")
    Call<List<RoutePlan>> getRoutePlanByUser(@Body User user);
    @GET("/routeplan/delete/{id}")
    Call<Void> deleteRouteplan(@Path("id") int id);
    @POST("/routeplan/getByUser/{date}")
    Call<List<RoutePlan>> getRoutePlanByUserAndDate(@Body User user,@Path("date") String date);

    @GET("/surveyanswer/getdate?")
    Call<List<SurveyAnswer>> getDateByUser(@Query("user") String user);
    @GET("/surveyanswer/getuser?")
    Call<List<SurveyAnswer>> getCustomerByUser(@Query("user") String user,@Query("date") String date);
    @GET("/surveyanswer/customer?")
    Call<List<SurveyAnswer>> getAnswerByCusomerAndUser(@Query("user") String user,@Query("customer") String customer,@Query("date") String date);

    @Multipart
    @POST("/user/saveWithPhoto")
    Call<String> saveWithPicture (@Part("user") RequestBody user, @Part MultipartBody.Part file);
}

package com.example.test.Model.Controller.Interface;

import com.example.test.Model.Tagging;

import java.util.List;

public interface UpdateListTagging {
    void UpdateList(List<Tagging> list);
}

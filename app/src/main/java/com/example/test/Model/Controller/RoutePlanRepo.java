package com.example.test.Model.Controller;

import com.example.test.Model.Controller.Interface.UpdateListRoutePlan;
import com.example.test.Model.Controller.Interface.UpdateListSurvey;
import com.example.test.Model.Repo.APIService;
import com.example.test.Model.Repo.APIUtils;
import com.example.test.Model.RoutePlan;
import com.example.test.Model.User;
import com.example.test.View.interfaces.ViewRoutePlan;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoutePlanRepo {
    APIService apiService;
    List data;
    UpdateListRoutePlan callback;
    public RoutePlanRepo(UpdateListRoutePlan callback){
        apiService = APIUtils.getAPIService();
        data = new ArrayList();
        this.callback = callback;
    }
    public void saveRoutePlan(RoutePlan routePlan){
        apiService.saveRoutePlan(routePlan).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {

                getRoutePlanByUserAndDate(routePlan.getUser(),routePlan.getDate().toString());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
    public  void getRouteByUser(User user){
        apiService.getRoutePlanByUser(user).enqueue(new Callback<List<RoutePlan>>() {
            @Override
            public void onResponse(Call<List<RoutePlan>> call, Response<List<RoutePlan>> response) {
                callback.UpdateList(response.body());

            }

            @Override
            public void onFailure(Call<List<RoutePlan>> call, Throwable t) {

            }
        });
    }
    public  void updateRoute(RoutePlan routePlan, String date){
        apiService.saveRoutePlan(routePlan).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                getRoutePlanByUserAndDate(routePlan.getUser(),date);
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
    public  void getRoutePlanByUserAndDate(User user,String str){
        apiService.getRoutePlanByUserAndDate(user,str).enqueue(new Callback<List<RoutePlan>>() {
            @Override
            public void onResponse(Call<List<RoutePlan>> call, Response<List<RoutePlan>> response) {
                callback.UpdateList(response.body());
            }

            @Override
            public void onFailure(Call<List<RoutePlan>> call, Throwable t) {
            }
        });
    }
    public void deleteRoute(RoutePlan routePlan){
        apiService.deleteRouteplan(routePlan.getId()).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                getRoutePlanByUserAndDate(routePlan.getUser(),routePlan.getDate().toString());
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {

            }
        });
    }
}

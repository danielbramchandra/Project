package com.example.test.Model.Controller.Interface;

import com.example.test.Model.User;

import java.util.List;

public interface UpdateListUser {
    void UpdateList(List<User> list);
}
